package oc

import (
	"github.com/Shopify/go-lua"
)

const (
	ColorWhite = iota
	ColorOrange
	ColorMagenta
	ColorLightblue
	ColorYellow
	ColorLime
	ColorPink
	ColorGray
	ColorSilver
	ColorCyan
	ColorPurple
	ColorBlue
	ColorBrown
	ColorGreen
	ColorRed
	ColorBlack
)

func OpenColors(l *lua.State) {
	lua.Require(l, "colors", func(l *lua.State) int {
		l.CreateTable(16, 16)
		table := l.Top()

		l.PushInteger(ColorWhite)
		l.SetField(table, "white")
		l.PushInteger(ColorWhite)
		l.PushString("white")
		l.SetTable(table)

		l.PushInteger(ColorOrange)
		l.SetField(table, "orange")
		l.PushInteger(ColorOrange)
		l.PushString("orange")
		l.SetTable(table)

		l.PushInteger(ColorMagenta)
		l.SetField(table, "magenta")
		l.PushInteger(ColorMagenta)
		l.PushString("magenta")
		l.SetTable(table)

		l.PushInteger(ColorLightblue)
		l.SetField(table, "lightblue")
		l.PushInteger(ColorLightblue)
		l.PushString("lightblue")
		l.SetTable(table)

		l.PushInteger(ColorYellow)
		l.SetField(table, "yellow")
		l.PushInteger(ColorYellow)
		l.PushString("yellow")
		l.SetTable(table)

		l.PushInteger(ColorLime)
		l.SetField(table, "lime")
		l.PushInteger(ColorLime)
		l.PushString("lime")
		l.SetTable(table)

		l.PushInteger(ColorPink)
		l.SetField(table, "pink")
		l.PushInteger(ColorPink)
		l.PushString("pink")
		l.SetTable(table)

		l.PushInteger(ColorGray)
		l.SetField(table, "gray")
		l.PushInteger(ColorGray)
		l.PushString("gray")
		l.SetTable(table)

		l.PushInteger(ColorSilver)
		l.SetField(table, "silver")
		l.PushInteger(ColorSilver)
		l.PushString("silver")
		l.SetTable(table)

		l.PushInteger(ColorCyan)
		l.SetField(table, "cyan")
		l.PushInteger(ColorCyan)
		l.PushString("cyan")
		l.SetTable(table)

		l.PushInteger(ColorPurple)
		l.SetField(table, "purple")
		l.PushInteger(ColorPurple)
		l.PushString("purple")
		l.SetTable(table)

		l.PushInteger(ColorBlue)
		l.SetField(table, "blue")
		l.PushInteger(ColorBlue)
		l.PushString("blue")
		l.SetTable(table)

		l.PushInteger(ColorBrown)
		l.SetField(table, "brown")
		l.PushInteger(ColorBrown)
		l.PushString("brown")
		l.SetTable(table)

		l.PushInteger(ColorGreen)
		l.SetField(table, "green")
		l.PushInteger(ColorGreen)
		l.PushString("green")
		l.SetTable(table)

		l.PushInteger(ColorRed)
		l.SetField(table, "red")
		l.PushInteger(ColorRed)
		l.PushString("red")
		l.SetTable(table)

		l.PushInteger(ColorBlack)
		l.SetField(table, "black")
		l.PushInteger(ColorBlack)
		l.PushString("black")
		l.SetTable(table)

		return 1
	}, false)
}
