package oc

import (
	"testing"

	"github.com/Shopify/go-lua"
)

func TestUUID(t *testing.T) {
	l := lua.NewState()
	lua.OpenLibraries(l)
	OpenUUID(l)
	if err := lua.DoString(l, "require('uuid').next()"); err != nil {
		t.Error(err)
	}
}
