package oc

import (
	"testing"

	"github.com/Shopify/go-lua"
)

func TestOSSleep(t *testing.T) {
	l := lua.NewState()
	lua.OpenLibraries(l)
	InjectOSSleep(l)

	if err := lua.DoString(l, "os.sleep()"); err == nil {
		t.Error("Error should be thrown if no arguments passed.")
	}

	if err := lua.DoString(l, "os.sleep('a')"); err == nil {
		t.Error("Error should be thrown if non-numeric argument passed.")
	}

	if err := lua.DoString(l, "os.sleep(0.3)"); err != nil {
		t.Error(err)
	}
}
