local colors = require('colors')

assert(colors.white == 0)
assert(colors[0] == 'white')

assert(colors.orange == 1)
assert(colors[1] == 'orange')

assert(colors.magenta == 2)
assert(colors[2] == 'magenta')

assert(colors.lightblue == 3)
assert(colors[3] == 'lightblue')

assert(colors.yellow == 4)
assert(colors[4] == 'yellow')

assert(colors.lime == 5)
assert(colors[5] == 'lime')

assert(colors.pink == 6)
assert(colors[6] == 'pink')

assert(colors.gray == 7)
assert(colors[7] == 'gray')

assert(colors.silver == 8)
assert(colors[8] == 'silver')

assert(colors.cyan == 9)
assert(colors[9] == 'cyan')

assert(colors.purple == 10)
assert(colors[10] == 'purple')

assert(colors.blue == 11)
assert(colors[11] == 'blue')

assert(colors.brown == 12)
assert(colors[12] == 'brown')

assert(colors.green == 13)
assert(colors[13] == 'green')

assert(colors.red == 14)
assert(colors[14] == 'red')

assert(colors.black == 15)
assert(colors[15] == 'black')
