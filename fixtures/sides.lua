local sides = require('sides')

assert(sides.bottom == 0)
assert(sides.down == 0)
assert(sides.negy == 0)
assert(sides[0] == 'bottom')

assert(sides.top == 1)
assert(sides.up == 1)
assert(sides.posy == 1)
assert(sides[1] == 'top')

assert(sides.back == 2)
assert(sides.north == 2)
assert(sides.negz == 2)
assert(sides[2] == 'back')

assert(sides.front == 3)
assert(sides.south == 3)
assert(sides.posz == 3)
assert(sides.forward == 3)
assert(sides[3] == 'front')

assert(sides.right == 4)
assert(sides.west == 4)
assert(sides.negx == 4)
assert(sides[4] == 'right')

assert(sides.left == 5)
assert(sides.east == 5)
assert(sides.posx == 5)
assert(sides[5] == 'left')

assert(sides.unknown == 6)
assert(sides[6] == 'unknown')

assert(#sides == 6)

for k, v in ipairs(sides) do
  if k == 1 then
    assert(v == 'bottom')
  elseif k == 6 then
    assert(v == 'left')
  end
end
