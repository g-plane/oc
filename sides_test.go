package oc

import (
	"testing"

	"github.com/Shopify/go-lua"
)

func TestSides(t *testing.T) {
	l := lua.NewState()
	lua.OpenLibraries(l)
	OpenSides(l)
	if err := lua.DoFile(l, "fixtures/sides.lua"); err != nil {
		t.Error(err)
	}
}
