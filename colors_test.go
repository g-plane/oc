package oc

import (
	"testing"

	"github.com/Shopify/go-lua"
)

func TestColors(t *testing.T) {
	l := lua.NewState()
	lua.OpenLibraries(l)
	OpenColors(l)
	if err := lua.DoFile(l, "fixtures/colors.lua"); err != nil {
		t.Error(err)
	}
}
