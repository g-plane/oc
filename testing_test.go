package oc

import (
	"strings"
	"testing"

	"github.com/Shopify/go-lua"
)

func TestTesting(t *testing.T) {
	l := lua.NewState()
	lua.OpenLibraries(l)
	OpenTesting(l)

	if err := lua.DoString(l, `
	local t = require('testing')
	t.is(1, 1)
	`); err != nil {
		t.Error(err)
	}

	if err := lua.DoString(l, `
	local t = require('testing')
	t.is(1, 2)
	`); err == nil {
		t.Error("Assertion failed.")
	}

	err := lua.DoString(l, `
	local t = require('testing')
	t.is(1, 2, 'custom message')
	`)
	if !strings.Contains(err.Error(), "custom message") {
		t.Error("Custom message should be allowed.")
	}
}
