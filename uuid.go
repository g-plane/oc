package oc

import (
	"github.com/Shopify/go-lua"
	uuid "github.com/satori/go.uuid"
)

func OpenUUID(l *lua.State) {
	lua.Require(l, "uuid", func(l *lua.State) int {
		l.CreateTable(0, 1)
		mod := l.Top()

		l.PushGoFunction(func(ls *lua.State) int {
			value, err := uuid.NewV4()
			if err != nil {
				ls.PushFString("Failed to generate UUID: %s", err.Error())
				ls.Error()
			}

			ls.PushString(value.String())

			return 1
		})
		l.SetField(mod, "next")

		return 1
	}, false)
}
