package oc

import (
	"github.com/Shopify/go-lua"
	"github.com/logrusorgru/aurora"
)

func OpenTesting(l *lua.State) {
	lua.Require(l, "testing", func(l *lua.State) int {
		l.CreateTable(0, 4)
		t := l.Top()

		l.PushGoFunction(tIs)
		l.SetField(t, "is")

		return 1
	}, false)
}

func tIs(l *lua.State) int {
	value, expected := 1, 2
	if !l.Compare(value, expected, lua.OpEq) {
		if l.Top() == 3 {
			l.PushValue(3)
		} else {
			l.PushFString(
				"Expected %s, but received %s.",
				aurora.Green(toString(l, expected)).String(),
				aurora.Red(toString(l, value)).String(),
			)
		}
		l.Error()
	}
	return 0
}

func toString(l *lua.State, index int) string {
	if result, ok := lua.ToStringMeta(l, index); ok {
		return result
	} else {
		return "(not a stringifiable value)"
	}
}
