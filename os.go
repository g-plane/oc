package oc

import (
	"time"

	"github.com/Shopify/go-lua"
)

func InjectOSSleep(l *lua.State) {
	l.Global("os")
	os := l.Top()

	l.PushGoFunction(func(ls *lua.State) int {
		if duration, ok := ls.ToNumber(1); ok {
			time.Sleep(time.Duration(duration) * time.Second)
		} else {
			l.PushString("Duration should be a number.")
			l.Error()
		}
		return 0
	})

	l.SetField(os, "sleep")
}
