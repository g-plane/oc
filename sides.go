package oc

import "github.com/Shopify/go-lua"

const (
	SideBottom = iota
	SideTop
	SideBack
	SideFront
	SideRight
	SideLeft
	SideUnknown
)

func OpenSides(l *lua.State) {
	lua.Require(l, "sides", func(ls *lua.State) int {
		ls.CreateTable(7, 19)
		table := ls.Top()

		ls.PushInteger(SideBottom)
		ls.SetField(table, "bottom")
		ls.PushInteger(SideBottom)
		ls.SetField(table, "down")
		ls.PushInteger(SideBottom)
		ls.SetField(table, "negy")
		ls.PushInteger(SideBottom)
		ls.PushString("bottom")
		ls.SetTable(table)

		ls.PushInteger(SideTop)
		ls.SetField(table, "top")
		ls.PushInteger(SideTop)
		ls.SetField(table, "up")
		ls.PushInteger(SideTop)
		ls.SetField(table, "posy")
		ls.PushInteger(SideTop)
		ls.PushString("top")
		ls.SetTable(table)

		ls.PushInteger(SideBack)
		ls.SetField(table, "back")
		ls.PushInteger(SideBack)
		ls.SetField(table, "north")
		ls.PushInteger(SideBack)
		ls.SetField(table, "negz")
		ls.PushInteger(SideBack)
		ls.PushString("back")
		ls.SetTable(table)

		ls.PushInteger(SideFront)
		ls.SetField(table, "front")
		ls.PushInteger(SideFront)
		ls.SetField(table, "south")
		ls.PushInteger(SideFront)
		ls.SetField(table, "posz")
		ls.PushInteger(SideFront)
		ls.SetField(table, "forward")
		ls.PushInteger(SideFront)
		ls.PushString("front")
		ls.SetTable(table)

		ls.PushInteger(SideRight)
		ls.SetField(table, "right")
		ls.PushInteger(SideRight)
		ls.SetField(table, "west")
		ls.PushInteger(SideRight)
		ls.SetField(table, "negx")
		ls.PushInteger(SideRight)
		ls.PushString("right")
		ls.SetTable(table)

		ls.PushInteger(SideLeft)
		ls.SetField(table, "left")
		ls.PushInteger(SideLeft)
		ls.SetField(table, "east")
		ls.PushInteger(SideLeft)
		ls.SetField(table, "posx")
		ls.PushInteger(SideLeft)
		ls.PushString("left")
		ls.SetTable(table)

		ls.PushInteger(SideUnknown)
		ls.SetField(table, "unknown")
		ls.PushInteger(SideUnknown)
		ls.PushString("unknown")
		ls.SetTable(table)

		ls.CreateTable(0, 1)
		metatable := ls.Top()
		ls.PushGoFunction(func(ls *lua.State) int {
			ls.Global("ipairs")
			ls.CreateTable(6, 0)
			itertable := ls.Top()
			ls.PushInteger(1)
			ls.PushString("bottom")
			ls.SetTable(itertable)
			ls.PushString("top")
			ls.RawSetInt(itertable, 2)
			ls.PushString("back")
			ls.RawSetInt(itertable, 3)
			ls.PushString("front")
			ls.RawSetInt(itertable, 4)
			ls.PushString("right")
			ls.RawSetInt(itertable, 5)
			ls.PushString("left")
			ls.RawSetInt(itertable, 6)
			ls.Call(1, 3)
			return 3
		})
		ls.SetField(metatable, "__ipairs")
		ls.SetMetaTable(table)

		return 1
	}, false)
}
